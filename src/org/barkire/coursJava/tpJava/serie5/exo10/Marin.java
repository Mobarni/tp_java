package org.barkire.coursJava.tpJava.serie5.exo10;

import java.io.Serializable;

public class Marin implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8373065257897336535L;
	private String prenom;
	private int salaire;

	public Marin(){

	}

	public Marin(String nouveauNom,String nouveauPrenom, int nouveauSalaire){
		this.nom=nouveauNom;
		this.prenom=nouveauPrenom;
		this.salaire=nouveauSalaire;
	}

	public Marin(String nouveauNom, int nouveauSalaire){
		this(nouveauNom,"", nouveauSalaire);
	}


	public void setNom(String nouveauNom){
		this.nom=nouveauNom;
	}

	public String getNom(){
		return this.nom;
	}

	public void setPrenom(String nouveauPrenom){
		this.prenom=nouveauPrenom;
	}

	public String getPrenom(){
		return this.prenom;
	}

	public void setSalaire(int nouveauSalaire){
		this.salaire=nouveauSalaire;
	}

	public int getSalaire(){
		return this.salaire;
	}

	public void augmenteSalaire(int augmentation){
		this.salaire+=augmentation;
	}


	public String toString(){
		return" [Nom: "+ this.getNom()+"\t"+ "Prenom: "+this.getPrenom()+"\t Salaire: "+ this.getSalaire()+"]";
	}

	private String nom;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + salaire;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Marin other = (Marin) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (salaire != other.salaire)
			return false;
		return true;
	}


}
