package org.barkire.coursJava.tpJava.serie5.exo10;

import java.io.*;
import java.util.*;



public class Sauvegarde {
	
	/**
	 * Le constructeur vide
	 */
	public Sauvegarde(){
		
	}

	/**
	 * Methode qui permet d'enregistrer les donn�es d'un marin dans un fichier texte
	 * @param nomFichier
	 * @param marin
	 */
	public static void sauveFichierTexte(String nomFichier, Marin marin){
		File fichier= new File(nomFichier);
		
		Writer writer=null;
		try{
			writer= new FileWriter(fichier, true);
			StringBuffer sb = new StringBuffer();
			sb.append(marin.getNom()).append("|").append(marin.getPrenom()).append("|").append(marin.getSalaire()).append("\r\n");
			writer.write(sb.toString());
		}
		catch(IOException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(writer!=null){
				try{
					writer.close();
				}
				catch(IOException e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * M�thode qui permet cr�er une liste de marins � partir du contenu d'un fichier 
	 * @param nomFichier
	 * @return
	 */
	public static List<Marin>  lisFichierTexte(String nomFichier){
		File fichier= new File(nomFichier);
		Reader fr=null;
		LineNumberReader lnr = null;
		Marin m;
		List<Marin> listeMarins=new ArrayList<Marin>();
		try{
			fr=new FileReader(fichier);
			lnr= new LineNumberReader(fr);
			
			String ligne;
			String champs[] = null;
			
			ligne=lnr.readLine();
			while(ligne!=null){
				champs=ligne.split("\\|");
				m=new Marin(champs[0],champs[1],Integer.parseInt(champs[2]));
				listeMarins.add(m);
				ligne=lnr.readLine();
			}
			
		}
		catch(IOException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(fr!=null&&lnr!=null){
				try{
					fr.close();
					lnr.close();
				}
				catch(IOException e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
		return listeMarins;		
	}
	
	/**
	 * M�thode qui permet d'enregistrer les donn�es d'un marin dans un fichier sous format binaire
	 * @param nomFichier
	 * @param m
	 */
	public static void sauveChampBinaire(String nomFichier, Marin m){
		OutputStream os=null;
		DataOutputStream dos=null;
		File fichier=null;
		try{
			fichier=new File(nomFichier);
			os= new FileOutputStream(fichier,true);
			dos= new DataOutputStream(os);
			
			dos.writeUTF(m.getNom());
			dos.writeUTF(m.getPrenom());
			dos.writeInt(m.getSalaire());
			dos.writeChars("\n");
			
		}
		catch(IOException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(os!=null&&dos!=null){
				try{
					os.close();
					dos.close();
				}
				catch(IOException e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * M�thode qui permet de cr�er une liste de marins � partir d'un fichier contenant des donn�es sous format binaire
	 * @param nomFichier
	 * @return
	 */
	public static List<Marin>  lisChampBinaire(String nomFichier){
		List<Marin> marins= new ArrayList<Marin>();
		InputStream is=null;
		DataInputStream dis=null;
		File fichier=null;
		try{
			fichier=new File(nomFichier);
			is= new FileInputStream(fichier);
			dis= new DataInputStream(is);
			
			while(is.available()>0){
				Marin m=new Marin();
				m.setNom(dis.readUTF());
				m.setPrenom(dis.readUTF());
				m.setSalaire(dis.readInt());
				dis.readChar();
				marins.add(m);
			}			
		}
		catch(IOException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(is!=null&&dis!=null){
				try{
					is.close();
					dis.close();
				}
				catch(IOException e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
		return marins;
	}
	
	/**
	 * M�thode qui permet d'enregistrer les �l�ments d'un �quipage sous forme d'objets
	 * @param nomFichier
	 * @param marins
	 */
	public static void sauveObject(String nomFichier, List<Marin> marins){
		OutputStream os=null;
		ObjectOutputStream oos=null;
		File fichier=null;
		try{
			fichier=new File(nomFichier);
			os= new FileOutputStream(fichier);
			oos= new ObjectOutputStream(os);
			for(int i=0;i<marins.size();i++){
			oos.writeObject(marins.get(i));
			}
		}
		catch(IOException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(os!=null&&oos!=null){
				try{
					os.close();
					oos.close();
				}
				catch(IOException e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * M�thode qui peermet d'obtenir une liste d'objets � partir des donn�es d'un fichier
	 * @param nomFichier
	 * @return
	 * @throws ClassNotFoundException
	 */
	public static List<Marin>  lisObject(String nomFichier) throws ClassNotFoundException{
		List<Marin> marins= new ArrayList<Marin>();
		InputStream is=null;
		ObjectInputStream ois=null;
		File fichier=null;
		try{
			fichier=new File(nomFichier);
			is= new FileInputStream(fichier);
			ois= new ObjectInputStream(is);
			
			while(is.available()>0){
				Marin m=new Marin();
				m=(Marin) ois.readObject();
				marins.add(m);
			}			
		}
		catch(IOException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(is!=null&&ois!=null){
				try{
					is.close();
					ois.close();
				}
				catch(IOException e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
		return marins;
	}
}
