package org.barkire.coursJava.tpJava.serie5.exo10;



import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		
		
		/* Cr�ation des marins */
		Marin m1= new Marin("Nadjib","Achir", 15000);
		Marin m2= new Marin("MOUSSA", "Traor�", 30000);
		Marin m3= new Marin("Medo", "Diouf", 5000);
		Marin m4= new Marin("TAMBA", "Bocoum", 20000);
		Marin m5= new Marin("ISSA", "Ide", 10000);
		
		Marin m6= new Marin("DIOP", "Diallo", 17900);
		Marin m7= new Marin("FAYE", "Traor�", 325000);
		Marin m8= new Marin("BA", "Diouf", 50600);
		Marin m9= new Marin("SY", "Bocoum", 90000);
		Marin m10= new Marin("SOW", "Ide", 30000);
		
		List<Marin> listeMarins = new ArrayList<Marin> ();
		String nomFichier="test.txt";
		Sauvegarde.sauveFichierTexte(nomFichier, m1);
		Sauvegarde.sauveFichierTexte(nomFichier, m2);
		Sauvegarde.sauveFichierTexte(nomFichier, m3);
		Sauvegarde.sauveFichierTexte(nomFichier, m4);
		Sauvegarde.sauveFichierTexte(nomFichier, m5);
		Sauvegarde.sauveFichierTexte(nomFichier, m6);
		Sauvegarde.sauveFichierTexte(nomFichier, m7);
		Sauvegarde.sauveFichierTexte(nomFichier, m8);
		Sauvegarde.sauveFichierTexte(nomFichier, m9);
		Sauvegarde.sauveFichierTexte(nomFichier, m10);
		
		listeMarins=Sauvegarde.lisFichierTexte(nomFichier);
		System.out.println("Liste des marins issus du fichier texte"+listeMarins);
		
		nomFichier="testbinaire.bin";
		Sauvegarde.sauveChampBinaire(nomFichier, listeMarins.get(0));
		Sauvegarde.sauveChampBinaire(nomFichier, listeMarins.get(1));
		Sauvegarde.sauveChampBinaire(nomFichier, listeMarins.get(2));
		Sauvegarde.sauveChampBinaire(nomFichier, listeMarins.get(3));
		Sauvegarde.sauveChampBinaire(nomFichier, listeMarins.get(4));
		Sauvegarde.sauveChampBinaire(nomFichier, listeMarins.get(5));
		Sauvegarde.sauveChampBinaire(nomFichier, listeMarins.get(6));
		Sauvegarde.sauveChampBinaire(nomFichier, listeMarins.get(7));
		Sauvegarde.sauveChampBinaire(nomFichier, listeMarins.get(8));
		Sauvegarde.sauveChampBinaire(nomFichier, listeMarins.get(9));
		
		
		System.out.println("Liste des marins issus du fichier binaire"+Sauvegarde.lisChampBinaire(nomFichier));
	
		nomFichier="testobject.txt";
		
		Sauvegarde.sauveObject(nomFichier, listeMarins);
		
		System.out.println("Liste des marins issus du fichier object"+Sauvegarde.lisObject(nomFichier));
		
		File f1,f2,f3;
		f1=new File("test.txt");
		f2=new File("testbinaire.bin");
		f3=new File("testobject.txt");
		System.out.println("La taille du fichier texte:"+f1.length()+"\n");
		System.out.println("La taille du fichier binaire:"+f2.length()+"\n");
		System.out.println("La taille du fichier object:"+f3.length()+"\n");
	}

}
