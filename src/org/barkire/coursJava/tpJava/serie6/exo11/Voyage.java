package org.barkire.coursJava.tpJava.serie6.exo11;

import java.util.*;

public class Voyage {
	private String depart;
	private String destination;
	private int jours;
	public static List<Voyage> voyages= new ArrayList<Voyage>();
	
	/**
	 * Le constructeur vide
	 */
	public Voyage(){
	
	}
	
	/**
	 * Le deuxi�me constructeur
	 * @param nouveauDepart
	 * @param nouvelleDestination
	 * @param nouveauJours
	 */
	public Voyage(String nouveauDepart, String nouvelleDestination, int nouveauJours){
		 this.depart=nouveauDepart;
		 this.destination=nouvelleDestination;
		 this.jours=nouveauJours;
		 voyages.add(this);
	}
	
	/**
	 * M�thode qui permet d'obbtenir le point de d�part d'un trajet
	 * @return
	 */
	public String getDepart() {
		return this.depart;
		
	}
	
	/**
	 * Affichage des donn�es d'un trajet
	 */
	@Override
	public String toString() {
		return "Voyage [depart=" + depart + ", destination=" + destination + ", jours=" + jours + "]";
	}

	/**
	 * Affichage de la destination d'un trajet
	 * @return
	 */
	public String getDestination() {
		return this.destination;
	}
	
	/**
	 * M�thode qui permet d'obtenir la dur�e d'un trajet
	 * @return
	 */
	public int getJours() {
		return this.jours;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((depart == null) ? 0 : depart.hashCode());
		result = prime * result + ((destination == null) ? 0 : destination.hashCode());
		result = prime * result + ((jours == 0) ? 0 : jours);
		return result;
	}

	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Voyage other = (Voyage) obj;
		if (depart == null) {
			if (other.depart != null)
				return false;
		} else if (!depart.equals(other.depart))
			return false;
		if (destination == null) {
			if (other.destination != null)
				return false;
		} 
			
		else if (!destination.equals(other.destination))
			return false;
		if(!depart.equals(other.destination)&&!destination.equals(other.depart))
			return false;
		return true;
	}

	public static Voyage getVoyage(String dep, String arr){
		for(Voyage v:voyages){
			if(v.getDepart().equals(dep)){
				if(v.getDestination().equals(arr)){
					return v;
				}
			}
		}
		
		return null;
	}
	public static List<Voyage> getVoyageAuDepartDe(String dep){
		List<Voyage> voyag= new ArrayList<Voyage>();
		for(Voyage v:voyages){
			if(v.getDepart().equals(dep)){
				voyag.add(v);
			}
		}
		return voyag;
	}
	public static Voyage getPlusCourtAuDepartDe(String dep){
		int i=0, delaiCourt;
		List<Voyage> listVoyage= new ArrayList<Voyage>();
		listVoyage=Voyage.getVoyageAuDepartDe(dep);
		int [] tabJours= new int[listVoyage.size()];
		for(Voyage v:listVoyage){
			if(v.getDepart().equals(dep)){
				tabJours[i]=v.getJours();
				i++;
			}
		}
		
		Arrays.sort(tabJours);
		delaiCourt=tabJours[0];
		for(Voyage v:voyages){
			if(v.getDepart().equals(dep)){
				if(v.getJours()==delaiCourt){
					return v;
				}
			}
		}
		return null;
	}
	
	public static Voyage NewInstance(String newDepart, String newDestination, int newJours){
		for(Voyage v:voyages){
			if(v.getDepart().equals(newDepart)){
				if(v.getDestination().equals(newDestination)){
					if(v.getJours()==newJours){
						return v;
					}
				}
			}
		}
		Voyage v1=new Voyage(newDepart, newDestination, newJours);
		return v1;
	}
	
}
