package org.barkire.coursJava.tpJava.serie6.exo11;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		/**
		 * Cr�ation des trajets 
		 * */
		Voyage v1= new Voyage("Paris","Marseille", 15);
		Voyage v2= new Voyage("Dijon", "Lyon", 3);
		Voyage v3= new Voyage("Bordeaux", "Lille", 5);
		Voyage v4= new Voyage("Nice", "Toulon", 2);
		Voyage v5= new Voyage("Rouen", "Lens", 1);
		
		Voyage v6= new Voyage("Paris","Toulouse", 20);
		Voyage v7= new Voyage("Toulouse", "Saint-Etienne", 9);
		Voyage v8= new Voyage("Bordeaux", "Lille", 19);
		Voyage v9= new Voyage("Paris", "Toulon", 27);
		Voyage v10= new Voyage("Montpellier", "Lens", 30);
		
		/**
		 * Affichage du trajet suivant son d�part et sa destination
		 */
		System.out.println(Voyage.getVoyage("Toulouse", "Saint-Etienne"));
		
		/**
		 * R�cup�ration de tous les trajets au d�part de Paris
		 */
		List<Voyage> voyages= new ArrayList<Voyage>();
		voyages=Voyage.getVoyageAuDepartDe("Paris");
		System.out.println(voyages);
		
		/**
		 * Affichage du trajet le plus court au d�part de Paris
		 */
		Voyage voyageCourt;
		voyageCourt= Voyage.getPlusCourtAuDepartDe("Paris");
		System.out.println(voyageCourt);
		
		/**
		 * Affichage du trajet entre Montpellier et Lens
		 */
		System.out.println(Voyage.NewInstance("Montpellier", "Lens", 30));
		
		/**
		 * Affichage de tous les trajets
		 */
		System.out.println(Voyage.voyages);
	}
}
