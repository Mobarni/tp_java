package org.barkire.coursJava.tpJava.serie6.exo12;

import java.util.ArrayList;
import java.util.List;

import org.barkire.coursJava.tpJava.serie6.exo11.Voyage;

public class Main {
	
	public static void main(String [] args){

		List<Voyage> listeVoyages= new ArrayList<Voyage>();
		String nomFichier="Voyages.txt";
		/**
		 * Affichage des donn�es contenues dans le fichier
		 */
		LireVoyages.lectureVoyages(nomFichier);
		
		/**
		 * Cr�ation d'une liste de trajets � partir des donn�es d'un fichier
		 */
		listeVoyages=LireVoyages.ecritureVoyages(nomFichier);
		System.out.println("Liste des marins issues de la lecture:\n"+listeVoyages);
	}
}
