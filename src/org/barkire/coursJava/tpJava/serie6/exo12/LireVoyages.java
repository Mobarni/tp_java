package org.barkire.coursJava.tpJava.serie6.exo12;

import java.io.*;
import java.util.*;
import org.barkire.coursJava.tpJava.serie6.exo11.Voyage;

public class LireVoyages {

	/**
	 * Le constructeur vide 
	 */
	public LireVoyages(){
		
	}
	
	/**
	 * La m�thode qui permet d'afficher les donn�es des trajets enresgistr�s dans un fichier texte
	 * @param nomFichier
	 */
	public static void lectureVoyages(String nomFichier){
		File fichier= new File(nomFichier);
		Reader fr=null;
		LineNumberReader lnr = null;
		try{
			fr=new FileReader(fichier);
			lnr= new LineNumberReader(fr);
			
			String ligne;
			
			ligne=lnr.readLine();
			while(ligne!=null){
				System.out.println(ligne);
				ligne=lnr.readLine();
			}
			
		}
		catch(IOException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(fr!=null&&lnr!=null){
				try{
					fr.close();
					lnr.close();
				}
				catch(IOException e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * M�thode qui permet de cr�er une liste de plusieurs trajets � partir des donn�es d'un fichier
	 * @param nomFichier
	 * @return
	 */
	public static List<Voyage> ecritureVoyages(String nomFichier){
		File fichier= new File(nomFichier);
		//List<Voyage> listeVoyages= new ArrayList<Voyage>();
		Reader fr=null;
		LineNumberReader lnr = null;
		String [] str;
		
		try{
			fr=new FileReader(fichier);
			lnr= new LineNumberReader(fr);
			
			String ligne;
			
			ligne=lnr.readLine();
			str=ligne.split(",");
			
			while(ligne!=null){
				int jours=Integer.parseInt(str[2]);
				Voyage.NewInstance(str[0], str[1], jours);
				ligne=lnr.readLine();
				if(ligne!=null)
					str=ligne.split(",");
			}
			
		}
		catch(IOException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(fr!=null&&lnr!=null){
				try{
					fr.close();
					lnr.close();
				}
				catch(IOException e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
		return Voyage.voyages;
	}
}
