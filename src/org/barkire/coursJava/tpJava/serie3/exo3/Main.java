package org.barkire.coursJava.tpJava.serie3.exo3;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* 
		 * Cr�ation des marins 
		 * */
		Marin m1= new Marin("SAMBA", "Diallo", 15000);
		Marin m2= new Marin("MOUSSA", "Traor�", 30000);
		Marin m3= new Marin("Medo", "Diouf", 5000);
		Marin m4= new Marin("TAMBA", "Bocoum", 20000);
		Marin m5= new Marin("ISSA", "Ide", 10000);
		
		
		/*
		 *  Cr�ation des capitaines 
		 *  */
		Capitaine c1= new Capitaine(Grade.BOSCO,"AOUTA", "Moussa", 800000);
		Capitaine c2= new Capitaine(Grade.CAPITAINE,"NIANDOU", "Ladji", 200000);
		Capitaine c3= new Capitaine(Grade.BOSCO,"HAROUNA", "Tinni", 400000);
		Capitaine c4= new Capitaine(Grade.CAPITAINE,"NIANDOU", "Ladji", 200000);
		
		/* 
		 * Cr�ation des �quipages
		 *  */
		Equipage e1= new Equipage();
		
		/* 
		 * Affichage des capitaines
		 *  */
		System.out.println("Le capitaine est:\t"+ c1.toString()+"\n");
		System.out.println("Le capitaine est:\t"+ c2.toString()+"\n");
		
		
		/*
		 * Test de l'�galit� entre deux capitaines
		 */
		
		if(c1.equals(c2)){
			System.out.println("Les deux capitaines "+c1.toString()+"et"+c2.toString()+"sont identiques\n");
		}
		else
			System.out.println("Les deux capitaines"+c1.toString()+"et"+c2.toString()+" sont differents\n");
		
		
		if(c4.equals(c2)){
			System.out.println("Les deux capitaines"+c2.toString()+"et"+c4.toString()+" sont identiques\n");
		}
		else
			System.out.println("Les deux capitaines"+c2.toString()+"et"+c4.toString()+" sont differents\n");
		
		
		/*
		 * Affichage des hashcodes de toutes les instances de capitaines
		 */
		System.out.println("Les hashcodes sont respectivement:\n");
		System.out.println(c1.hashCode()+"\n"+c2.hashCode()+"\n"+c3.hashCode()+"\n"+c4.hashCode()+"\n");
		
		
		
		
		/* 
		 * Constitution d'un �quipage
		 * */
		e1.addMarin(c1);
		e1.addMarin(c2);
		e1.addMarin(m3);
		e1.addMarin(m4);
		e1.addMarin(m5);
		
		/* 
		 * Affichage de l'�quipage constitu�
		 * */
		System.out.println(e1.toString()+"\n");
		
		
		
	}

}
