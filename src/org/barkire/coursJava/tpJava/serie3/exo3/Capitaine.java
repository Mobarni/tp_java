package org.barkire.coursJava.tpJava.serie3.exo3;

public class Capitaine extends Marin {
	private Grade grade;

	/*
	 * Le constructeur vide
	 */
	public Capitaine(){
		
	}
	
	/*
	 * Le deuxi�me constructeur 
	 */
	public Capitaine(Grade g, String nouveauNom,String nouveauPrenom,int nouveauSalaire){
		super(nouveauNom, nouveauPrenom, nouveauSalaire);
		this.grade=g;		
	}
	
	/*
	 * Renvoie les donn�es d'une instance de capitaine
	 */
	public String toString(){
		String variable;
		variable="["+grade+ super.toString()+"]";
		return variable;
	}
	
	
	public boolean equals(Capitaine c){
		if(this.grade.equals(c.grade)){
			if(super.equals(c)){
				return true;
			}	
		}
		return false;
	}
	 
	public int hashCode(){
		int nbre=31, code=1;
		code= code*nbre+this.grade.hashCode();
		code= code*nbre+super.hashCode();
		return code;
	}


}
