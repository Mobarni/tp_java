package org.barkire.coursJava.tpJava.serie3.exo5;

import java.util.Arrays;

public class Equipage {

	private Marin [] marins= new Marin [5];
	
	public Equipage(){
		
	}
	
	public Equipage(Capitaine c){
		this.addMarin(c);
	}
	
	public int getCapacity(){
		int capacite;
		capacite=marins.length;
		return capacite;
	}
	public int getNombreMarins(){
		int nombre=0;
		for(int i=0; i<this.getCapacity();i++){
			if(null!=marins[i]){
				nombre++;
			}
		}
		return nombre;
	}
	
	public boolean addMarin(Marin m){
		if(null!=m){
			if(this.getCapacity()>this.getNombreMarins()){
				if(!this.isMarinPresent(m)){
				marins[this.getNombreMarins()]=m;
				return true;
			}
			}
		}
		return false;
	}
	public String toString(){
		String variable;
		variable="[Equipage:";
		for(int i=0; i<this.getNombreMarins();i++){
			if(null!=marins[i]){
			variable+=marins[i].toString()+"\t";
			}
		}
		variable+="]";
		return variable;
		
	}
	public boolean isMarinPresent(Marin m){
		if(null!=m){
			for(int j=0; j<this.getCapacity();j++){
				if(m.equals(marins[j]))
					return true;
			}
		}
		return false;
	}
	public boolean removeMarin(Marin m){
		if(null!=m){
			for(int k=0; k<this.getCapacity();k++){
				if(this.isMarinPresent(m)){
					marins[k]=null;
					return true;
				}
			}
		}
		return false;
	}
	public Marin[] getEquipage(){
		Marin [] marinsPresent= new Marin[this.getNombreMarins()];
		for(int i=0; i<this.getNombreMarins();i++){
				marinsPresent[i]=marins[i];
		}
		return marinsPresent;
	}

	public boolean clear(){
		for(int i=0; i<this.getCapacity();i++){
				marins[i]=null;
		}
		return true;
	}
	
	public boolean addAllEquipage(Equipage e){
		Marin [] marins2;
		int placeDisponible;
		placeDisponible= this.getCapacity()- this.getNombreMarins();
		marins2=e.getEquipage();
		if(placeDisponible>=e.getNombreMarins()){
			for(int i=0; i<e.getNombreMarins();i++){
				this.addMarin(marins2[i]);
			}
			return true;
		}
		return false;
	}

	public void entendEquipage(int nouvellesPlaces){
		int longueur;
		longueur=this.getCapacity()+nouvellesPlaces;
		Marin [] newTab= new Marin[longueur];
		System.arraycopy(this.marins, 0, newTab, 0, this.getCapacity());
		marins=newTab;
	}
	
	public boolean equals(Equipage e){
		for(int i=0; i<this.getNombreMarins();i++){
			if(!this.isMarinPresent(e.marins[i]))
				return false;
		}
		return true;
	}
	
	public int hashCode(){
		int [] tabhash= new int[this.getNombreMarins()];
		int nbre=31, code=1;
		for(int i=0; i<this.getNombreMarins();i++){
			tabhash[i]=this.marins[i].hashCode();
		}
		Arrays.sort(tabhash);
		for(int i=0; i<this.getNombreMarins();i++){
		code= code*nbre+tabhash[i];
		}
		return code;
	}
}

