package org.barkire.coursJava.tpJava.serie3.exo5;

public class BateauAMoteur extends Bateau{
	private String propulsion="Moteur";
	
	/*
	 * Le constructeur vide
	 * */
	public BateauAMoteur(){
		
	}
	
	/*
	 * Le deuxieme constructeur 
	 * */
	public BateauAMoteur(String nouveauNom, int nouveauTonnage){
		super(nouveauNom, nouveauTonnage);
	}
	
	/*
	 * Le troisi�me constructeur 
	 * */
	public BateauAMoteur(String nouveauNom, int nouveauTonnage, EquipageCommande nouvelEquipage){
		super(nouveauNom, nouveauTonnage, nouvelEquipage);
	}
	
	/*
	 * La m�thode qui permet d'obtenir le type de propulsion du bateau
	 * */
	public String getPropulsion(){
		return this.propulsion;
	}
	
	/*
	 * Affichage des donn�es
	 * */
	public String toString() {
		return "["+ super.toString()+ "Propulsion:"+propulsion +"]";
	}

}
