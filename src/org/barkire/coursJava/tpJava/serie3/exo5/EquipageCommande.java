package org.barkire.coursJava.tpJava.serie3.exo5;

public class EquipageCommande extends Equipage {
	private Capitaine commandant;
	
	public EquipageCommande(Capitaine c){
		this.commandant=c;
	}
	
	public String toString(){
		String variable;
		variable="[EquipageCommande:"+commandant.toString()+ super.toString()+"]";
		return variable;
	}
	
	public boolean equals(EquipageCommande e){
		if(this.commandant.equals(e.commandant)){
			if(super.equals(e)){
				return true;
			}	
		}
		return false;
	}
	
	public int hashCode(){
		int nbre=31, code=1;
		code= code*nbre+this.commandant.hashCode();
		code= code*nbre+super.hashCode();
		return code;
	}
}
