package org.barkire.coursJava.tpJava.serie3.exo5;

public class Bateau {
	String nom;
	int tonnage;
	EquipageCommande equipage;
	
	/* 
	 * Le constructeur vide
	 * */
	public Bateau(){
		
	}
	
	/*
	 * Le deuxieme constructeur
	 */
	public Bateau(String nouveauNom, int nouveauTonnage){
		nom=nouveauNom;
		tonnage=nouveauTonnage;
		
	}
	
	/*
	 * Le troisi�me constructeur
	 **/
	public Bateau(String nouveauNom, int nouveauTonnage, EquipageCommande nouvelEquipage){
		nom=nouveauNom;
		tonnage=nouveauTonnage;
		equipage=nouvelEquipage;
		
	}
	
	/*
	 * La m�thode qui permet d'avoir le nom du bateau
	 **/
	public String getNom() {
		return nom;
	}
	
	/*
	 * La m�thode qui permet d'avoir le tonnage du bateau
	 **/
	public int getTonnage() {
		return tonnage;
	}
	
	/*
	 * La m�thode qui permet d'obtenir l'�quipage du bateau
	 **/
	public EquipageCommande getEquipage() {
		return equipage;
	}

	/*
	 * L'�quipage qui permet de d�finir l'�quipage du bateau
	 **/
	public void setEquipage(EquipageCommande equipage) {
		this.equipage = equipage;
	}
	
	/*
	 * Les m�thodes de test d'�galit� entre deux bateaux
	 * */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + this.tonnage;
		result = prime * result + equipage.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bateau other = (Bateau) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	/*
	 * L'affichage des donn�es du bateau
	 **/
	public String toString() {
		return "Bateau [nom=" + nom + ", tonnage=" + tonnage + ", equipage=" + equipage.toString() + "]";
	}

	
}
