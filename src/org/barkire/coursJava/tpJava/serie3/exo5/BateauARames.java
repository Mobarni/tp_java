package org.barkire.coursJava.tpJava.serie3.exo5;

public class BateauARames extends Bateau{
	private String propulsion="Rames";
	
	/*
	 * Le constructeur vide
	 * */
	public BateauARames(){
		
	}
	
	/*
	 *  Le  deuxieme constructeur 
	 */
	public BateauARames(String nouveauNom, int nouveauTonnage){
		super(nouveauNom, nouveauTonnage);
	}
	
	/*
	 * Le troisi�me constructeur
	 */
	public BateauARames(String nouveauNom, int nouveauTonnage, EquipageCommande nouvelEquipage){
		super(nouveauNom, nouveauTonnage, nouvelEquipage);
	}
	
	/*
	 * La m�thode qui permet d'obtenir le type de propulsion
	 * */
	public String getPropulsion(){
		return this.propulsion;
	}
	
	/*
	 * Affichage des donn�es
	 * */
	public String toString() {
		return "["+ super.toString()+ "Propulsion:"+propulsion +"]";
	}
}
