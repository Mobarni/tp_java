package org.barkire.coursJava.tpJava.serie3.exo5;



public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/* 
		 * Cr�ation des marins 
		 * */
		Marin m1= new Marin("SAMBA", "Diallo", 15000);
		Marin m2= new Marin("MOUSSA", "Traor�", 30000);
		Marin m3= new Marin("Medo", "Diouf", 5000);
		Marin m4= new Marin("TAMBA", "Bocoum", 20000);
		Marin m5= new Marin("ISSA", "Ide", 10000);
		
		Marin m6= new Marin("DIOP", "Diallo", 17900);
		Marin m7= new Marin("FAYE", "Traor�", 325000);
		Marin m8= new Marin("BA", "Diouf", 50600);
		Marin m9= new Marin("SY", "Bocoum", 90000);
		Marin m10= new Marin("SOW", "Ide", 30000);
		
		/* 
		 * Cr�ation des capitaines 
		 * */
		Capitaine c1= new Capitaine(Grade.BOSCO,"AOUTA", "Moussa", 800000);
		Capitaine c2= new Capitaine(Grade.CAPITAINE,"NIANDOU", "Ladji", 200000);
		
		/* 
		 * Cr�ation des �quipages 
		 * */
		EquipageCommande e= new EquipageCommande(c1);
		EquipageCommande e2= new EquipageCommande(c2);
		
		/* 
		 * Constitution des �quipages 
		 * */
		
		/* 1er Equipage */
		e.addMarin(m1);
		e.addMarin(m2);
		e.addMarin(m3);
		e.addMarin(m4);
		e.addMarin(m5);
		
		/* 2e Equipage */
		e2.addMarin(m6);
		e2.addMarin(m7);
		e2.addMarin(m8);
		e2.addMarin(m9);
		e2.addMarin(m10);

		/*
		 * Initialisation du bateau
		 */
		Bateau B1= new Bateau("Lamartine", 500, e);
		BateauARames B2= new BateauARames("Dossolais", 800);
		B2.setEquipage(e2);
		/*BateauARames B3= new BateauARames();
		BateauAVoiles B4= new BateauAVoiles();*/
		
		/*
		 *  Affichage des donn�es des bateaux
		 */
		System.out.println(B1.toString());
		System.out.println(B2.toString());
	}

}
