package org.barkire.coursJava.tpJava.serie3.exo5;

public class BateauAVoiles extends Bateau{
private String propulsion="Voiles";
	
	/*
	 * Le constructeur vide
	 */
	public BateauAVoiles(){
	
	}
	
	/*
	 * Le deuxieme constructeur 
	 */
	public BateauAVoiles(String nouveauNom, int nouveauTonnage){
		super(nouveauNom, nouveauTonnage);
	}

	/*
	 * Le troisieme constructeur
	 */
	public BateauAVoiles(String nouveauNom, int nouveauTonnage, EquipageCommande nouvelEquipage){
		super(nouveauNom, nouveauTonnage, nouvelEquipage);
	}

	/*
	 * La m�thode qui donne le type de propulsion
	 */
	public String getPropulsion(){
		return this.propulsion;
	}
	
	/*
	 * Affichage des donn�es
	 * */
	public String toString() {
		return "["+ super.toString()+ "Propulsion:"+propulsion +"]";
	}
}
