package org.barkire.coursJava.tpJava.serie1.exo5;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * Initialisation des marins
		 */
		Marin m1= new Marin("SAMBA", "Diallo", 15000);
		Marin m2= new Marin("MOUSSA", "Traor�", 30000);
		Marin m3= new Marin("Medo", "Diouf", 5000);
		Marin m4= new Marin("TAMBA", "Bocoum", 20000);
		Marin m5= new Marin("ISSA", "Ide", 10000);
		
		/*
		 * Initialisation d'un tableau de marins
		 */
		Marin [] marins= {m1,m2,m3,m4,m5};
		
		/*
		 * Cr�ation d'une instance de MarinUtil
		 */
		MarinUtil mU= new MarinUtil();
		
		/*
		 * Augmentation du salaire de chaque marin de 20%
		 */
		mU.augmenteSalaire(marins, 20);
		
		/*
		 * Affichage des donn�es de chaque marin apr�s modification
		 */
		System.out.println("Le premier marin est:\n"+ m1.toString());
		System.out.println("\nLe deuxieme marin est:\n"+ m2.toString());
		System.out.println("\nLe troisieme marin est:\n"+ m3.toString()+"\n");
	}

}
