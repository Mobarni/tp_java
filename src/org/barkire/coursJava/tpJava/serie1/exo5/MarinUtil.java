package org.barkire.coursJava.tpJava.serie1.exo5;

import java.util.Arrays;

public class MarinUtil extends Marin{
	/*
	 * Le constructeur vide 
	 */
	public MarinUtil(){
		
	}
	
	/*
	 * Methode qui augmente le salaire � plusieurs marins
	 */
	public void augmenteSalaire(Marin[] marins, int pourcentage){
		int salaire;
		
		for(int i=0; i<marins.length;i++){
			salaire=marins[i].getSalaire();
			marins[i].augmenteSalaire(pourcentage*salaire/100);
		}
	}
	
	/*
	 * M�thode qui permet d'obtenir le salaire le plus �l�v�
	 */
	public int getMaxSalaire(Marin [] marins){
		int [] tab= new int[marins.length];
		for(int i=0; i<marins.length;i++){
			tab[i]=marins[i].getSalaire();
		}
		
		Arrays.sort(tab);
		return tab[marins.length-1];
	}
	
	/*
	 * M�thode qui permet d'obtenir le salaire moyen des marins
	 */
	public int getMoyenneSalaire(Marin [] marins){
		int somme=0, moyenne;
		for(int i=0; i<marins.length;i++){
			somme+=marins[i].getSalaire();
		}
		moyenne=somme/marins.length;
		return moyenne;
	}
	
	/*
	 * M�thode qui permet d'obtenir la valeur m�diane des salaires
	 */
	public int getMedianeSalaire(Marin [] marins){
		int [] tab= new int[marins.length];
		for(int i=0; i<marins.length;i++){
			tab[i]=marins[i].getSalaire();
		}
		
		Arrays.sort(tab);
		return tab[marins.length/2];
	}
}
