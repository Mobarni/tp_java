package org.barkire.coursJava.tpJava.serie1.exo4;

public class Marin{
	private String nom;
	private String prenom;
	private int salaire;
	
	/*
	 * D�finition du constructeur vide
	 */
	public Marin(){
		
	}
	
	/*
	 * D�finition du 2e constructeur 
	 */
	public Marin(String nouveauNom,String nouveauPrenom, int nouveauSalaire){
		this.nom=nouveauNom;
		this.prenom=nouveauPrenom;
		this.salaire=nouveauSalaire;
	}
	
	/*
	 * D�finition du 3e constructeur 
	 */
	public Marin(String nouveauNom, int nouveauSalaire){
		this(nouveauNom,"", nouveauSalaire);
	}
	
	/*
	 * Methode permettant de d�finir un nom � une instance de marin
	 */
	public void setNom(String nouveauNom){
		this.nom=nouveauNom;
	}
	
	/*
	 * Methode permettant de lire un nom d'une instance de marin
	 */
	public String getNom(){
		return this.nom;
	}
	
	/*
	 * Methode permettant de d�finir un prenom � une instance de marin
	 */
	public void setPrenom(String nouveauPrenom){
		this.nom=nouveauPrenom;
	}
	
	/*
	 * Methode permettant de lire un pr�nom d'une instance de marin
	 */
	public String getPrenom(){
		return this.prenom;
	}
	
	/*
	 * Methode permettant de d�finir un salaire � une instance de marin 
	 */
	public void setSalaire(int nouveauSalaire){
		this.salaire=nouveauSalaire;
	}
	
	/*
	 * Methode permettant de lire un salaire d'une instance de marin
	 */
	public int getSalaire(){
		return this.salaire;
	}

	/*
	 * Methode permettant d'augmenter un salaire d'une instance de marin
	 */
	public void augmenteSalaire(int augmentation){
		this.salaire+=augmentation;
	}


	/*
	 * Methode permettant de lire les caracteristiques d'une instance de marin
	 */
	public String toString(){
		return" [Nom: "+ this.getNom()+"\t"+ "Prenom: "+this.getPrenom()+"\t Salaire: "+ this.getSalaire()+"]";
	}
	
	/*
	 * Methode permettant de comparer deux instances de marin
	 */
	public boolean equals(Marin m){
		if(this.nom==m.nom){
			if(this.prenom==m.prenom){
				if(this.salaire==m.salaire){
					return true;
				}
			}
		}
		return false;
	}
	
	/*
	 * Methode permettant de lire un pr�nom d'une instance de marin
	 */
	public int hashcode(){
		int nbre=31, code=1;
		code= code*nbre+this.nom.hashCode();
		code= code*nbre+this.prenom.hashCode();
		code= code*nbre+this.getSalaire();
		return code;
	}
}
