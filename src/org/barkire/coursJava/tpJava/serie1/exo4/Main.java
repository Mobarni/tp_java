package org.barkire.coursJava.tpJava.serie1.exo4;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * Initialisation des Marins
		 */
		Marin m1= new Marin("SAMBA", "Diallo", 15000);
		Marin m2= new Marin("SAMBA", "Diallo", 15000);
		Marin m3= new Marin("MOUSSA", "Traor�", 15000);
		
		/*
		 * Affichage des donn�es sur les marins
		 */
		System.out.println("Le premier marin est:\n"+ m1.toString());
		System.out.println("\nLe deuxieme marin est:\n"+ m2.toString());
		System.out.println("\nLe troisieme marin est:\n"+ m3.toString()+"\n");
	
    /*
     * Test si deux marins sont identiques
     */
	if(m1.equals(m2)){
		System.out.println("Les deux marins" + m1.toString()+"et"+m2.toString()+"sont pareils");
	}
	else{
		
		System.out.println("Les deux marins" + m1.toString()+"et"+m2.toString()+"sont differents");
	}
	
	if(m1.equals(m3)){
		System.out.println("Les deux marins" + m1.toString()+"et"+m3.toString()+"sont pareils");
	}
	else{
		System.out.println("Les deux marins" + m1.toString()+"et"+m3.toString()+"sont differents");
		System.out.println("Donc les deux marins" + m2.toString()+"et"+m3.toString()+"sont differents aussi");
	}
	
	/*
	 * Affichage du hashcode de chaque marin
	 */
	System.out.println("Le hashcode du premier marin est:\n"+ m1.hashcode());
	System.out.println("\nLe hashcode du deuxieme marin est:\n"+ m2.hashcode());
	System.out.println("\nLe hashcode du troisieme marin est:\n"+ m3.hashcode()+"\n");
	
	}
}