package org.barkire.coursJava.tpJava.serie7.exo13;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

public class LectureFilm {

	/**
	 * M�thode qui permet d'afficher le contenu d'un fichier
	 * @param nomFichier
	 */
	public static void lecture(String nomFichier){
		File fichier= new File(nomFichier);
		Reader fr=null;
		LineNumberReader lnr = null;
		try{
			fr=new FileReader(fichier);
			lnr= new LineNumberReader(fr);
			
			String ligne;
			ligne=lnr.readLine();
			while(ligne!=null){
				System.out.println(ligne);
				ligne=lnr.readLine();
			}
			
		}
		catch(IOException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(fr!=null&&lnr!=null){
				try{
					fr.close();
					lnr.close();
				}
				catch(IOException e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * M�thode qui permet de cr�er une liste de film � partir d'un fichier contenant des donn�es
	 * @param nomFichier
	 * @return
	 */
	public static Set<Film> ecritureFilm(String nomFichier){
		File fichier= new File(nomFichier);
		Reader fr=null;
		LineNumberReader lnr = null;
		
		try{
			fr=new FileReader(fichier);
			lnr= new LineNumberReader(fr);
			Film film;
			String ligne;
			String titre = null;
			int annee = 0;
			ligne=lnr.readLine();
			StringTokenizer st= new StringTokenizer(ligne,"/");
			List<String> acteurs = new ArrayList<String>();
			while(st.hasMoreTokens()){
				String var=st.nextToken();
				titre=var.substring(0, var.lastIndexOf(" "));
				annee=Integer.parseInt(var.substring(var.lastIndexOf(" ")+2,var.lastIndexOf(" ")+6));
				
				do{
					
					String nom = null, prenom = null;
					nom=st.nextToken();
					
					//System.out.println(titre+" "+annee+" "+nom);
				
					/*
					StringBuilder sb=new StringBuilder();
					if(nom!=null&&prenom!=null){
						sb.append(prenom).append(nom);
						nom=sb.toString();
						
					}*/
					acteurs.add(nom);
				}
				while(st.hasMoreTokens());
					
				ligne=lnr.readLine();
				if(ligne!=null){
					st= new StringTokenizer(ligne,"/");
				}
				film=new Film(titre,annee,acteurs);
				
			}
		}
		catch(IOException e){
			System.out.println("Erreur"+e.getMessage());
			e.printStackTrace();
		}
		finally{
			if(fr!=null&&lnr!=null){
				try{
					fr.close();
					lnr.close();
				}
				catch(IOException e){
					System.out.println("Erreur"+e.getMessage());
					e.printStackTrace();
				}
			}
		}		
		return Film.listeFilms;
	}
}
