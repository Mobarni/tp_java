package org.barkire.coursJava.tpJava.serie7.exo13;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Film {
	private String titre;
	private int annee;
	private List<String> acteurs;
	public static Set<Film> listeFilms = new HashSet<Film>();
	
	/**
	 * Le constructeur vide
	 */
	public Film(){
		
	}
	
	/**
	 * Le deuxi�me constructeur
	 * @param nTitre
	 * @param nAnnee
	 * @param nActeurs
	 */
	public Film(String nTitre,int nAnnee, List<String>  nActeurs){
		this.titre=nTitre;
		this.annee=nAnnee;
		this.acteurs=nActeurs;
		listeFilms.add(this);
	}
	
	/**
	 * M�thode qui permet d'obtenir le titre d'un film
	 * @return
	 */
	public String getTitre() {
		return titre;
	}
	
	/**
	 * M�thode qui d�finir le titre d'un film
	 * @param titre
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	/***
	 * M�thode qui permet d'obtenir l'ann�e du film
	 * @return
	 */
	public int getAnnee() {
		return annee;
	}
	
	/**
	 * M�thode qui permet de d�finir l'ann�e d'un film
	 * @param annee
	 */
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	
	/**
	 * M�thode qui permet d'obtenir les acteurs d'un film
	 * @return
	 */
	public List<String> getActeurs() {
		return acteurs;
	}
	
	/**
	 * M�thode qui permet de d�finir les acteurs d'un film
	 * @param acteurs
	 */
	public void setActeurs(List<String>  acteurs) {
		this.acteurs = acteurs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((acteurs == null) ? 0 : acteurs.hashCode());
		result = prime * result + annee;
		result = prime * result + ((titre == null) ? 0 : titre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Film other = (Film) obj;
		if (acteurs == null) {
			if (other.acteurs != null)
				return false;
		} else if (!acteurs.equals(other.acteurs))
			return false;
		if (annee != other.annee)
			return false;
		if (titre == null) {
			if (other.titre != null)
				return false;
		} else if (!titre.equals(other.titre))
			return false;
		return true;
	}

	/**
	 * M�thode qui permet d'obtenir les donn�es d'un film
	 */
	@Override
	public String toString() {
		return "Film [titre=" + titre + ", annee=" + annee + ", acteurs=" + acteurs + "]";
	}

	
}
