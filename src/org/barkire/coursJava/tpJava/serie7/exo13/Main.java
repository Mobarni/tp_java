package org.barkire.coursJava.tpJava.serie7.exo13;

import java.util.HashSet;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Set<Film> listeFilms = new HashSet<Film>();
		String nomFichier="movies-mpaa.txt";
		//LectureFilm.lecture(nomFichier);
		/**
		 * Cr�ation d'une liste de film � partir des donn�es d'un fichier
		 */
		listeFilms=LectureFilm.ecritureFilm(nomFichier);
		
		/**
		 * Affichage des donn�es de l'ensemble des films
		 */
		for(Film f:listeFilms){
		System.out.println(f);
		System.out.println("\n");
		}
	}
}
