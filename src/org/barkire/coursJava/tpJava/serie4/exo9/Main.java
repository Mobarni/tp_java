 package org.barkire.coursJava.tpJava.serie4.exo9;

import org.barkire.coursJava.tpJava.serie4.exo8.MarinComparator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/**
		 * Cr�ation des marins 
		 * */
		MarinComparator m1= new MarinComparator("SAMBA", "Diallo");
		MarinComparator m2= new MarinComparator("MOUSSA", "Traor�");
		MarinComparator m3= new MarinComparator("MEDO", "Diouf");
		
		MarinComparator m6= new MarinComparator("SAMBA", "Diallo");
		MarinComparator m7= new MarinComparator("MOUSSA", "Traor�");
		MarinComparator m8= new MarinComparator("BA", "Diouf");
		
		/**
		 * Cr�ation d'un �quipage
		 */
		EquipageNew equipage= new EquipageNew();
		
		equipage.addMarin(m1);
		equipage.addMarin(m2);
		equipage.addMarin(m3);
		equipage.addMarin(m6);
		equipage.addMarin(m7);
		equipage.addMarin(m8);
		
		/**
		 * Affichage de l'�quipage
		 */
		System.out.println(equipage);
		
		/**
		 * Affichage d'un marin � partir de son nom
		 */
		System.out.println(equipage.getMarinByName("SAMBA"));
	}

}

