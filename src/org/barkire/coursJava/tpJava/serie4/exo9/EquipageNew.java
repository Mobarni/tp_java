package org.barkire.coursJava.tpJava.serie4.exo9;

import java.util.SortedSet;
import java.util.TreeSet;

import org.barkire.coursJava.tpJava.serie4.exo8.MarinComparator;

public class EquipageNew  {

private SortedSet<MarinComparator> marins= new TreeSet<MarinComparator>();
	
	
	
	@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((marins == null) ? 0 : marins.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	EquipageNew other = (EquipageNew) obj;
	if (marins == null) {
		if (other.marins != null)
			return false;
	} else if (!marins.equals(other.marins))
		return false;
	return true;
}

	/**
	 * M�thode qui d�termine le nombre de marins dans la liste
	 */
	public int getNombreMarins(){
		return marins.size();
	}
	
	/**
	 * M�thode addMarin qui rajoute un marin dans un �quipage
	 * @param m
	 * @return
	 */
	public boolean addMarin(MarinComparator m){
		if(null!=m){
			if(!this.isMarinPresent(m)){
					marins.add(m);
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * M�thode permet d'afficher les donn�es d'un �quipage
	 * 
	 */
	public String toString(){
		return marins.toString();
	}
	
	/**
	 * M�thode qui teste la pr�sence d'un marin
	 * @param m
	 * @return
	 */
	public boolean isMarinPresent(MarinComparator m){
		if(null!=m){
			return marins.contains(m);
			}
		return false;
	}
	
	/**
	 * M�thode qui permet de supprimer un marin d'un �quipage
	 * @param m
	 * @return
	 */
	public boolean removeMarin(MarinComparator m){
		if(null!=m){
			return marins.remove(m);
		}
		return false;
	}

	/**
	 * M�thode qui permet d'obtenir le contenu d'un �quipage
	 * @return
	 */
	public SortedSet<MarinComparator> getEquipage(){
		
		return this.marins;
	}

	/**
	 * M�thode qui permet d'effacer le contenu d'un �quipage
	 */
	public void clear(){
		marins.clear();
	}
	
	/**
	 * M�thode qui permet de transf�rer tout ou une partie du contenu d'un �quipage dans un autre
	 * @param e
	 * @return
	 */
	public boolean addAllEquipage(EquipageNew e){
		return marins.addAll(e.getEquipage());
	}
	
	/**
	 * M�thode qui permet d'obtenir un marin � partir de son nom
	 * @param nom
	 * @return
	 */
	public MarinComparator getMarinByName(String nom){
		for(MarinComparator m: this.marins){
			if(null!=m){
				if(m.getNom().equals(nom))
					return m;
			}
		}
		return null;
		
	}
}
