 package org.barkire.coursJava.tpJava.serie4.exo8;

public class ComparableMarin implements Comparable<ComparableMarin>{
	private String nom;
	private String prenom;
	
	/**
	 * Le constructeur vide
	 */
	public ComparableMarin(){
		
	}
	
	/**
	 * Le deuxi�me constructeur
	 * @param nouveauNom
	 * @param nouveauPrenom
	 */
	public ComparableMarin(String nouveauNom,String nouveauPrenom){
		this.nom=nouveauNom;
		this.prenom=nouveauPrenom;
	}
	
	/**
	 * Le troisi�me constructeur
	 * @param nouveauNom
	 */
	public ComparableMarin(String nouveauNom){
		this(nouveauNom,"");
	}
	
	/**
	 * La m�thode qui permet de d�finir un nom � un marin
	 * @param nouveauNom
	 */
	public void setNom(String nouveauNom){
		this.nom=nouveauNom;
	}
	
	/**
	 * M�thode qui permet d'obtenir le nom d'un marin
	 * @return
	 */
	public String getNom(){
		return this.nom;
	}
	
	/**
	 * M�thode qui permet de d�finir le prenom d'un marin
	 * @param nouveauPrenom
	 */
	public void setPrenom(String nouveauPrenom){
		this.nom=nouveauPrenom;
	}
	
	/**
	 * La m�thode qui permet d'avoir le pr�nom d'un marin
	 * @return
	 */
	public String getPrenom(){
		return this.prenom;
	}
	

	/**
	 * M�thode qui permet de tester l'�galit� de deux marins
	*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ComparableMarin other = (ComparableMarin) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		return true;
	}
 
	public String toString(){
		return" [Nom: "+ this.getNom()+"\t"+ "Prenom: "+this.getPrenom()+"]";
	}

	public int compareTo(ComparableMarin m1){
		if(null!=m1){
			if(this.getNom().equals(m1.getNom())){
				return getPrenom().compareTo(m1.getPrenom());
			}
		else
			return this.getNom().compareTo(m1.getNom());
	}
		else
			return -1;
}	
}
