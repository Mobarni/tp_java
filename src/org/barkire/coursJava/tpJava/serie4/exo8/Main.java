 package org.barkire.coursJava.tpJava.serie4.exo8;

import java.util.*;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* Cr�ation des marins */
		MarinComparator m1= new MarinComparator("SAMBA", "Diallo");
		MarinComparator m2= new MarinComparator("MOUSSA", "Traor�");
		MarinComparator m3= new MarinComparator("MEDO", "Diouf");
		
		MarinComparator m6= new MarinComparator("SAMBA", "Diallo");
		MarinComparator m7= new MarinComparator("MOUSSA", "Traor�");
		MarinComparator m8= new MarinComparator("BA", "Diouf");
		
		SortedSet<MarinComparator> equipage= new TreeSet<MarinComparator>();
		
		equipage.add(m1);
		equipage.add(m2);
		equipage.add(m3);
		equipage.add(m6);
		equipage.add(m7);
		equipage.add(m8);
		
		System.out.println(equipage);
		
	}

}
