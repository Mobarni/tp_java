package org.barkire.coursJava.tpJava.serie4.exo8;

import java.util.Comparator;

import org.barkire.coursJava.tpJava.serie4.exo7.Marin;

public class MarinComparator extends Marin implements Comparator<Marin>{

	/**
	 * Le constructeur vide
	 */
	public MarinComparator(){
		
	}
	
	/**
	 * Le deuxi�me constructeur 
	 * @param nouveauNom
	 * @param nouveauPrenom
	 */
	public MarinComparator(String nouveauNom, String nouveauPrenom){
		super(nouveauNom, nouveauPrenom);
	}

	/**
	 * La m�thode qui compare deux marins
	 */
	public int compare(Marin m1, Marin m2) {
		if(null!=m1){
			if(null!=m2){
				return m1.compareTo(m2);
			}
		}
		return -1;
	}

}
