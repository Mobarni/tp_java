package org.barkire.coursJava.tpJava.serie4.exo6;

import org.barkire.coursJava.tpJava.serie2.exo2.Marin;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/**
		 *  Cr�ation des marins 
		 * */
		Marin m1= new Marin("SAMBA", "Diallo", 15000);
		Marin m2= new Marin("MOUSSA", "Traor�", 30000);
		Marin m3= new Marin("Medo", "Diouf", 5000);
		Marin m4= new Marin("TAMBA", "Bocoum", 20000);
		Marin m5= new Marin("ISSA", "Ide", 10000);
		
		Marin m6= new Marin("DIOP", "Diallo", 17900);
		Marin m7= new Marin("FAYE", "Traor�", 325000);
		Marin m8= new Marin("BA", "Diouf", 50600);
		Marin m9= new Marin("SY", "Bocoum", 90000);
		Marin m10= new Marin("SOW", "Ide", 30000);
		
		/**
		 * Cr�ation des �quipages
		 * */
		Equipage e1= new Equipage();
		Equipage e2= new Equipage();
		Equipage e3= new Equipage();
		Equipage e4= new Equipage();
		
		/*
		 *  Constitution des �quipages 
		 * */
		
		/* 1er Equipage */
		e1.addMarin(m1);
		e1.addMarin(m2);
		e1.addMarin(m3);
		e1.addMarin(m4);
		e1.addMarin(m5);
		
		/* 2e Equipage */
		e2.addMarin(m5);
		e2.addMarin(m1);
		e2.addMarin(m2);
		e2.addMarin(m4);
		e2.addMarin(m3);
		
		/* 3e Equipage */
		e3.addMarin(m6);
		e3.addMarin(m7);
		e3.addMarin(m8);
		e3.addMarin(m9);
		e3.addMarin(m10);
		
		/* 4e Equipage */
		e4.addMarin(m1);
		e4.addMarin(m2);
		e4.addMarin(m3);
		e4.addMarin(m4);
		e4.addMarin(m5);
		
		/* Affichage du pemier �quipage */
		System.out.println("Le nombre de marins du premier �quipage est:\t"+ e1.getNombreMarins());
		
		/* Suppression d'un marin du pemier �quipage */
		if(e1.removeMarin(m5)){
			System.out.println("Le"+ m5.toString()+"a �t� retir� du premier �quipage\n");
		}
		
		/* Affichage du nouveau contenu du pemier �quipage */
		System.out.println("Le nombre de marins du premier �quipage est:\t"+ e1.getNombreMarins());
		System.out.println(e1.toString()+"\n");
		
		/* Suppression du contenu du pemier �quipage */
		e1.clear();
		System.out.println("Le premier �quipage a �t� vid�\n");
		
		
		System.out.println("Le nombre de marins du premier �quipage est:\t"+ e1.getNombreMarins());
		System.out.println(e1.toString()+"\n");
		
		/**
		 * Transfert du quatri�me �quipage dans le premier
		 */
		e1.addAllEquipage(e4);
		
		/**
		 * Affichage du contenu du premier �quipage
		 */
		System.out.println("La nombre de marins du premier �quipage est:\t"+ e1.getNombreMarins());
		System.out.println(e1.toString()+"\n");
		
		/**
		 * Test de l'�galit� entre deux �quipage
		 */
		if(e1.equals(e2)){
			System.out.println("Le premier �quipage et le deuxieme sont identiques\n");
		}
		else
			System.out.println("Le premier �quipage et le deuxieme sont differents\n");
		
		if(e1.equals(e3)){
			System.out.println("Le premier �quipage et le deuxieme sont identiques\n");
		}
		else
			System.out.println("Le premier �quipage et le deuxieme sont differents\n");
		
		System.out.println(e1.hashcode()+"\n"+e2.hashcode()+"\n"+e3.hashcode()+"\n");
		
	}

}
