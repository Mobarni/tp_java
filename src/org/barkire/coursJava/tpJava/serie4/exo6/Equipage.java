package org.barkire.coursJava.tpJava.serie4.exo6;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.barkire.coursJava.tpJava.serie2.exo2.Marin;



public class Equipage {

	private List<Marin> marins= new ArrayList<Marin>();
	
	ListIterator<Marin> li= this.marins.listIterator();
	
	/**
	 * Le constructeur vide
	 */
	public Equipage(){
		
	}
	
	/**
	 * M�thode qui d�termine le nombre de marins dans la liste
	 */
	public int getNombreMarins(){
		return marins.size();
	}
	
	/**
	 * M�thode addMarin qui rajoute un marin dans un equipage
	 * @param m
	 * @return boolean
	 */
	public boolean addMarin(Marin m){
		if(null!=m){
			if(!this.isMarinPresent(m)){
					marins.add(m);
				return true;
			}
		}
		return false;
	}
	
	
	/**
	 * M�thode qui permet d'avoir les donn�es d'un �quipage
	 * 
	 */
	public String toString(){
		return marins.toString();
	}
	
	/**
	 * M�thode qui permet de v�rifier la pr�sence d'un marin ddans un �quipage
	 * @param m
	 * @return
	 */
	public boolean isMarinPresent(Marin m){
		if(null!=m){
			return marins.contains(m);
			}
		return false;
	}
	
	/**
	 * M�thode qui permet de supprimer un marin d'un �quipage
	 * @param m
	 * @return
	 */
	public boolean removeMarin(Marin m){
		if(null!=m){
			return marins.remove(m);
		}
		return false;
	}
	
	/**
	 * M�thode qui permet de fournir le contenu d'un �quipage
	 * @return
	 */
	public List<Marin> getEquipage(){
		return this.marins;
	}

	/**
	 * M�thode qui permet d'effacer le contenu d'un �quipage
	 */
	public void clear(){
		marins.clear();
	}
	
	/**
	 * M�thode qui permet le transferer le contenu d'un �quipage dans un autre
	 * @param e
	 * @return
	 */
	public boolean addAllEquipage(Equipage e){
		return marins.addAll(e.getEquipage());
	}
	
	/**
	 * M�thode qui permet de tester l'�galit� de deux �quipages
	 * @param e
	 * @return
	 */
	public boolean equals(Equipage e){
		if(this.getNombreMarins()==e.getNombreMarins()){
			ListIterator<Marin> it= e.getEquipage().listIterator();
			while(it.hasNext()){
				if(!marins.contains(it.next()))
					return false;
			}
			return true;
		}
		else
			return false;
	}
		
	
	public int hashcode(){
		return marins.hashCode();
	}
}

