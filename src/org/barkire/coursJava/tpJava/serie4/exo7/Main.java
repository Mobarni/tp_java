package org.barkire.coursJava.tpJava.serie4.exo7;

import java.util.HashSet;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/* Cr�ation des marins */
		Marin m1= new Marin("SAMBA", "Diallo");
		Marin m2= new Marin("MOUSSA", "Traor�");
		Marin m3= new Marin("Medo", "Diouf");
		
		Marin m6= new Marin("SAMBA", "Diallo");
		Marin m7= new Marin("MOUSSA", "Traor�");
		Marin m8= new Marin("BA", "Diouf");
		
		Set<Marin> equipage= new HashSet<Marin>();
		
		equipage.add(m1);
		equipage.add(m2);
		equipage.add(m3);
		equipage.add(m6);
		equipage.add(m7);
		equipage.add(m8);
		
		System.out.println(equipage);
		
	}

}
