package org.barkire.coursJava.tpJava.serie2.exo2;

public class Marin{
	private String nom;
	private String prenom;
	private int salaire;
	
	public Marin(){
		
	}
	
	public Marin(String nouveauNom,String nouveauPrenom, int nouveauSalaire){
		this.nom=nouveauNom;
		this.prenom=nouveauPrenom;
		this.salaire=nouveauSalaire;
	}
	
	public Marin(String nouveauNom, int nouveauSalaire){
		this(nouveauNom,"", nouveauSalaire);
	}
	
	
	public void setNom(String nouveauNom){
		this.nom=nouveauNom;
	}
	
	public String getNom(){
		return this.nom;
	}
	
	public void setPrenom(String nouveauPrenom){
		this.nom=nouveauPrenom;
	}
	
	public String getPrenom(){
		return this.prenom;
	}
	
	public void setSalaire(int nouveauSalaire){
		this.salaire=nouveauSalaire;
	}
	
	public int getSalaire(){
		return this.salaire;
	}

	public void augmenteSalaire(int augmentation){
		this.salaire+=augmentation;
	}


	public String toString(){
		return" [Nom: "+ this.getNom()+"\t"+ "Prenom: "+this.getPrenom()+"\t Salaire: "+ this.getSalaire()+"]";
	}
	
	public boolean equals(Marin m){
		if(null!=m){
			if(this.nom==m.nom){
				if(this.prenom==m.prenom){
					if(this.salaire==m.salaire){
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public int hashcode(){
		int nbre=31, code=1;
		code= code*nbre+this.nom.hashCode();
		code= code*nbre+this.prenom.hashCode();
		code= code*nbre+this.getSalaire();
		return code;
	}
}
