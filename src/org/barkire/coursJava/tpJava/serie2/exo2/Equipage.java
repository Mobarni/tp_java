package org.barkire.coursJava.tpJava.serie2.exo2;

import java.util.Arrays;

public class Equipage {

	/*
	 * Instanciation d'un tableau de marins
	 */
	private Marin [] marins= new Marin [5];
	
	/*
	 * M�thode qui permet de d�terminer la taille d'un �quipage
	 */
	public int getCapacity(){
		int capacite;
		capacite=marins.length;
		return capacite;
	}
	
	/*
	 * M�thode qui permet d'obtenir le nombre de marins effectivement pr�sent
	 */
	public int getNombreMarins(){
		int nombre=0;
		for(int i=0; i<this.getCapacity();i++){
			if(null!=marins[i]){
				nombre++;
			}
		}
		return nombre;
	}
	
	/*
	 * M�thode qui permet de rajouter un marin � un �quipage
	 */
	public boolean addMarin(Marin m){
		if(null!=m){
			if(this.getCapacity()>this.getNombreMarins()){
				if(!this.isMarinPresent(m)){
				marins[this.getNombreMarins()]=m;
				return true;
			}
			}
		}
		return false;
	}
	
	/*
	 * Affichage du contenu d'un �quipage
	 */
	public String toString(){
		String variable;
		variable="[Equipage:";
		for(int i=0; i<this.getNombreMarins();i++){
			if(null!=marins[i]){
			variable+=marins[i].toString()+"\t";
			}
		}
		variable+="]";
		return variable;
		
	}
	
	/*
	 * M�thode qui permet de v�rifier la pr�sence d'un marin dans un �quipage
	 */
	public boolean isMarinPresent(Marin m){
		if(null!=m){
			for(int j=0; j<this.getCapacity();j++){
				if(m.equals(marins[j]))
					return true;
			}
		}
		return false;
	}
	
	/*
	 * M�thode qui permet de supprimer un marin d'un �quipage
	 */
	public boolean removeMarin(Marin m){
		if(null!=m){
			for(int k=0; k<this.getCapacity();k++){
				if(this.isMarinPresent(m)){
					marins[k]=null;
					return true;
				}
			}
		}
		return false;
	}
	
	/*
	 * M�thode qui permet d'obtenir un �quipage
	 */
	public Marin[] getEquipage(){
		Marin [] marinsPresent= new Marin[this.getNombreMarins()];
		for(int i=0; i<this.getNombreMarins();i++){
				marinsPresent[i]=marins[i];
		}
		return marinsPresent;
	}

	/*
	 * M�thode qui permet d'effacer le contenu d'un �quipage
	 */
	public boolean clear(){
		for(int i=0; i<this.getCapacity();i++){
				marins[i]=null;
		}
		return true;
	}
	
	/*
	 * M�thode qui permet de charger tout ou une partie d'un �quipage dans un autre
	 */
	public boolean addAllEquipage(Equipage e){
		Marin [] marins2;
		int placeDisponible;
		placeDisponible= this.getCapacity()- this.getNombreMarins();
		marins2=e.getEquipage();
		if(placeDisponible>=e.getNombreMarins()){
			for(int i=0; i<e.getNombreMarins();i++){
				this.addMarin(marins2[i]);
			}
			return true;
		}
		return false;
	}

	/*
	 * M�thode qui permet d'�tendre un �quipage
	 */
	public void entendEquipage(int nouvellesPlaces){
		int longueur;
		longueur=this.getCapacity()+nouvellesPlaces;
		Marin [] newTab= new Marin[longueur];
		System.arraycopy(this.marins, 0, newTab, 0, this.getCapacity());
		marins=newTab;
	}
	
	/*
	 * M�thodes de comparaison de deux �quipages
	 */
	
	public boolean equals(Equipage e){
		for(int i=0; i<this.getNombreMarins();i++){
			if(!this.isMarinPresent(e.marins[i]))
				return false;
		}
		return true;
	}
	
	public int hashcode(){
		int [] tabhash= new int[this.getNombreMarins()];
		int nbre=31, code=1;
		for(int i=0; i<this.getNombreMarins();i++){
			tabhash[i]=this.marins[i].hashCode();
		}
		Arrays.sort(tabhash);
		for(int i=0; i<this.getNombreMarins();i++){
		code= code*nbre+tabhash[i];
		}
		return code;
	}
}

