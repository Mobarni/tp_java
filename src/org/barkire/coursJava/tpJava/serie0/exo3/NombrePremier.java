package org.barkire.coursJava.tpJava.serie0.exo3;

public class NombrePremier {

	
	/* 
	 * Constructeur vide de la classe NombrePremier
	 * */ 
	public NombrePremier(){
		
	}
	
	/*
	 *  M�thode qui vise � d�terminer un nomre premier
	 */
	public boolean premier(int nombre){
		
		for(int i=2;i<=Math.pow(nombre, 0.5);i++){
			if(nombre%i==0)
				return false;
		}
		return true;
	}
	
}
