package org.barkire.coursJava.tpJava.serie0.exo3;

public class Palindrome {

	/*
	 * Constructeur vide de la classe Palindrome
	 * */ 
	public Palindrome(){
		
	}
	
	/*
	 *  Definition de la methode Palindrome 
	 *  */
	public boolean palindrome(int nombre){
		String s;
		int i,longueur, mediane;
		
		/* 
		 * Conversion du nombre en cha�ne de caract�res 
		 * */ 
		s= String.valueOf(nombre);
		
		/* 
		 * Determination de la longueur du mot 
		 * */
		longueur=s.length();
		
		mediane=longueur/2;
		/*
		 *  Comparaison de chacune des lettres du mot symetrique � la position de la mediane 
		 *  */
		for(i=0;i<mediane;i++){
			/*
			 * Cas ou le nombre de caract�res est paire
			 */
			if(longueur%2==0){
				if(s.indexOf(mediane-i-1)!=s.indexOf(mediane+i))
					return false;
			}
			/*
			 * Cas ou le nombre de caract�res est paire
			 */
			else
			{
				if(s.indexOf(mediane-i)!=s.indexOf(mediane+i))
					return false;
			}
		}
		return true;
	}
}