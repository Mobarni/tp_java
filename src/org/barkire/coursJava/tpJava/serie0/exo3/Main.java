package org.barkire.coursJava.tpJava.serie0.exo3;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*
		 * Initialisation d'une variable entière
		 */
		int nbre=151;
		
		/* 
		 * Creation d'une instance de NombrePremier
		 * */
		NombrePremier testPremier= new NombrePremier();
		
		/*
		 *  Vérification si le nombre est premier 
		 *  */
		if(testPremier.premier(nbre)){
		System.out.println("Le nombre est premier \n");
		
		/* 
		 * Creation d'une instance de Palindrome
		 * */
		Palindrome p= new Palindrome();
		
		/* 
		 * Vérification si le nombre est aussi palindrome
		 * */
		if(p.palindrome(nbre)){
			System.out.println("Le nombre est palindrome \n");
		}
		else{
			System.out.println("Le nombre n'est pas palindrome \n");
			}
		}
		
		else{
			System.out.println("Le nombre n'est pas premier \n");
		}
	}  
}
