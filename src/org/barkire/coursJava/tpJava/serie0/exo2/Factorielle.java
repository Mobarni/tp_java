package org.barkire.coursJava.tpJava.serie0.exo2;
/*
 * Importation de la biblioth�que contenant la  classe BigInteger
 */
import java.math.BigInteger;

public class Factorielle {

	/*
	 * Le constructeur vide
	 */
	public Factorielle(){
		
	}
	
	/*
	 * Surcharge de la m�thode factorielle suivant le type INTEGER
	 */
	public int factorielle(int nombre){
		int i, result=1;
		for(i=1; i<=nombre; i++){
		result*=i;
		}
		return result;
	}
	
	/*
	 * Surcharge de la m�thode factorielle suivant le type DOUBLE
	 */
	public double factorielle(double nombre){
		int i;
		double result=1;
		for(i=1; i<=nombre; i++){
		result*=i;
		}
		return result;
	}
	
	/*
	 * Surcharge de la m�thode factorielle suivant le type BIGINTEGER
	 */
	public BigInteger factorielle(BigInteger nombre){
		BigInteger i, result= BigInteger.valueOf(1);
		for(i=BigInteger.valueOf(1); i.compareTo(nombre)<0;i=i.add(BigInteger.valueOf(1))){
		result=result.add(result.multiply(i));
		}
		return result;
	}
}
