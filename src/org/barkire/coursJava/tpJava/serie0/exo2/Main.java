package org.barkire.coursJava.tpJava.serie0.exo2;

import java.math.BigInteger;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Définition d'une instance de la classe factorielle
		 */
		Factorielle fact= new Factorielle();
		/*
		 * Déclaration de trois variables différentes pour les tests
		 */
		int a=10;
		double b=20;
		BigInteger c=BigInteger.valueOf(10);
		
		/*
		 * Résultats 
		 */
		System.out.println("Factorielle de 10="+ fact.factorielle(a));
		
		System.out.println("Factorielle de 20="+ fact.factorielle(b));
		
		System.out.println("Factorielle de 30="+ fact.factorielle(c));
			
	}

}
